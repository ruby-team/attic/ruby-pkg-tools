# =========================================================================
# Debian Ruby Packaging Tools 
# GNU Make class for auto-updating the Uploaders field
#
# $Id$
# =========================================================================

# Members list of Debian Ruby Maintainers
TEAM_LIST ?= /usr/share/ruby-pkg-tools/pkg-ruby-extras.team

# Remove maintainer from uploaders list
maintainer := $(shell sed -n 's/^Maintainer: //p' debian/control.in)
uploaders := $(shell grep -vF '$(maintainer)' $(TEAM_LIST))

clean::	
	$(warning Usage of the uploaders rule is DEPRECATED, please remove it!)
	if [ ! -f debian/control.in ] ; then \
		echo "No debian/control.in found.";\
		false;\
	fi
	sed "s/@RUBY_EXTRAS_TEAM@/$(uploaders)/" debian/control.in > debian/control
ifneq ($(shell grep '@RUBY_TEAM@' debian/control.in),)
	$(warning @RUBY_TEAM@ is deprecated, use @RUBY_EXTRAS_TEAM@ instead.)
	sed "s/@RUBY_TEAM@/$(uploaders)/" debian/control.in > debian/control
endif
