# -*- mode: makefile; coding: utf-8 -*-
# Copyright © 2005 Esteban Manchado Velázquez <zoso@debian.org>
# Copied from python-distutils.mk,
#             Copyright © 2003 Colin Walters <walters@debian.org>,
#             then adapted to Ruby conventions
# 
# Description: configure, compile, binary, and clean Ruby libraries and programs
#  This class works for Ruby packages which use the setup.rb script.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
# 02111-1307 USA.


ifndef _cdbs_bootstrap
_cdbs_scripts_path ?= /usr/lib/cdbs
_cdbs_rules_path ?= /usr/share/cdbs/1/rules
_cdbs_class_path ?= /usr/share/cdbs/1/class
endif

ifndef _cdbs_class_ruby_setup_rb
_cdbs_class_ruby_setup_rb := 1

include /usr/share/ruby-pkg-tools/1/class/ruby-common.mk

PACKAGED_RUBY_SETUP_CMD = /usr/lib/ruby/1.8/setup.rb
DEB_RUBY_SETUP_CMD = debian-setup.rb
DEB_RUBY_CONFIG_ARGS = --installdirs=std
DEB_RUBY_CLEAN_TARGET = distclean

# command to install symlink to packaged setup.rb: used in both build and clean
# targets
DEB_RUBY_INSTALL_SETUP_CMD = \
	if [ ! -L $(DEB_RUBY_SETUP_CMD) ] ; then \
		if [ -f $(DEB_RUBY_SETUP_CMD) ] ; then \
			mv $(DEB_RUBY_SETUP_CMD) $(DEB_RUBY_SETUP_CMD).moved_away_by_debian_build ;\
		fi ; \
		ln -s $(PACKAGED_RUBY_SETUP_CMD) $(DEB_RUBY_SETUP_CMD) ; \
	fi

DEB_RUBY_REMOVE_SETUP_CMD = \
	if [ -L $(DEB_RUBY_SETUP_CMD) ] ; then \
		rm $(DEB_RUBY_SETUP_CMD) ; \
	fi ; \
	if [ -f $(DEB_RUBY_SETUP_CMD).moved_away_by_debian_build ] ; then \
		mv $(DEB_RUBY_SETUP_CMD).moved_away_by_debian_build $(DEB_RUBY_SETUP_CMD) ; \
	fi

# Build simple packages
$(patsubst %,build/%,$(DEB_RUBY_SIMPLE_PACKAGES)) :: build/% :
	$(DEB_RUBY_INSTALL_SETUP_CMD)
	cd $(DEB_SRCDIR) && /usr/bin/ruby1.8 $(DEB_RUBY_SETUP_CMD) config $(DEB_RUBY_CONFIG_ARGS) && /usr/bin/ruby1.8 $(DEB_RUBY_SETUP_CMD) setup
	$(call push_cfg_file,$(cdbs_curpkg))


# Install regular library packages
$(patsubst %,install/%,$(DEB_RUBY_REAL_LIB_PACKAGES)) :: install/% :
	$(call pop_cfg_file,$(cdbs_curpkg))
	cd $(DEB_SRCDIR)
	$(DEB_RUBY_INSTALL_SETUP_CMD)
	/usr/bin/ruby$(cdbs_ruby_ver) $(DEB_RUBY_SETUP_CMD) config $(DEB_RUBY_CONFIG_ARGS)
	/usr/bin/ruby$(cdbs_ruby_ver) $(DEB_RUBY_SETUP_CMD) $(DEB_RUBY_CLEAN_TARGET)
	/usr/bin/ruby$(cdbs_ruby_ver) $(DEB_RUBY_SETUP_CMD) config $(DEB_RUBY_CONFIG_ARGS)
	/usr/bin/ruby$(cdbs_ruby_ver) $(DEB_RUBY_SETUP_CMD) setup
	/usr/bin/ruby$(cdbs_ruby_ver) $(DEB_RUBY_SETUP_CMD) install --prefix=debian/$(cdbs_curpkg)
	$(call push_cfg_file,$(cdbs_curpkg))

# Install simple packages
$(patsubst %,install/%,$(DEB_RUBY_SIMPLE_PACKAGES)) :: install/% :
	$(call pop_cfg_file,$(cdbs_curpkg))
	cd $(DEB_SRCDIR) && /usr/bin/ruby1.8 $(DEB_RUBY_SETUP_CMD) install --prefix=debian/$(cdbs_curpkg)
	$(call push_cfg_file,$(cdbs_curpkg))


clean::
	$(DEB_RUBY_INSTALL_SETUP_CMD)
	/usr/bin/ruby1.8 $(DEB_RUBY_SETUP_CMD) config $(DEB_RUBY_CONFIG_ARGS)
	/usr/bin/ruby1.8 $(DEB_RUBY_SETUP_CMD) $(DEB_RUBY_CLEAN_TARGET)
	rm -f $(DEB_SRCDIR)/.config-*
	$(DEB_RUBY_REMOVE_SETUP_CMD)

endif
