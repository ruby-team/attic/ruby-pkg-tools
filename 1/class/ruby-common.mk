# -*- mode: makefile; coding: utf-8 -*-
# Copyright © 2005 Esteban Manchado Velázquez <zoso@debian.org>
# Copied from python-distutils.mk,
#             Copyright © 2003 Colin Walters <walters@debian.org>,
#             then adapted to Ruby conventions
# 
# Description: configure, compile, binary, and clean Ruby libraries and programs
#  This class works for Ruby packages which use the setup.rb script.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
# 02111-1307 USA.

ifndef _cdbs_bootstrap
_cdbs_scripts_path ?= /usr/lib/cdbs
_cdbs_rules_path ?= /usr/share/cdbs/1/rules
_cdbs_class_path ?= /usr/share/cdbs/1/class
endif

ifndef _cdbs_class_ruby_common
_cdbs_class_ruby_common := 1

include $(_cdbs_class_path)/langcore.mk$(_cdbs_makefile_suffix)
include $(_cdbs_rules_path)/buildcore.mk$(_cdbs_makefile_suffix)

# Just be sure you include debhelper.mk before including this file.
ifdef _cdbs_rules_debhelper
# It's a bad idea to compress examples and tests in the docs
DEB_COMPRESS_EXCLUDE := .rb
endif


DEB_RUBY_COMPILE_VERSION = 

DEB_RUBY_VERSIONS = 1.8 1.9 1.9.1
DEB_RUBY_DEFAULT_VERSION = 1.8
DEB_RUBY_LIBDIR=$(strip $(shell ruby$(cdbs_ruby_ver) -rrbconfig -e 'puts Config::CONFIG["rubylibdir"]'))
DEB_RUBY_ARCHDIR=$(strip $(shell ruby$(cdbs_ruby_ver) -rrbconfig -e 'puts Config::CONFIG["archdir"]'))

# Separate lib packages from the rest
LIB_PACKAGE_FILTER ?= %-ruby $(patsubst %,\%-ruby%,$(DEB_RUBY_VERSIONS))
LIB_DOC_PACKAGE_FILTER ?= %-doc
DEB_RUBY_LIB_PACKAGES := $(strip $(filter $(LIB_PACKAGE_FILTER),$(DEB_ALL_PACKAGES)))
DEB_RUBY_REAL_LIB_PACKAGES := $(strip $(filter-out %-ruby, $(DEB_RUBY_LIB_PACKAGES)))
DEB_RUBY_DUMMY_LIB_PACKAGES := $(strip $(filter %-ruby, $(DEB_RUBY_LIB_PACKAGES)))
DEB_RUBY_LIB_DOC_PACKAGES := $(strip $(filter $(LIB_DOC_PACKAGE_FILTER),$(DEB_ALL_PACKAGES)))
ifeq (,$(DEB_RUBY_REAL_LIB_PACKAGES))
DEB_RUBY_SIMPLE_PACKAGES := $(strip $(filter-out $(LIB_PACKAGE_FILTER),$(DEB_ALL_PACKAGES)))
endif

cdbs_ruby_ver_aux = $(filter-out %-,$(subst -ruby,- ,$(cdbs_curpkg)))
cdbs_ruby_ver = $(if $(findstring $(cdbs_curpkg),$(cdbs_ruby_ver_aux)),$(DEB_RUBY_DEFAULT_VERSION),$(or $(cdbs_ruby_ver_aux),$(DEB_RUBY_DEFAULT_VERSION)))

push_cfg_file = if [ -f $(DEB_SRCDIR)/.config ]; then mv -f $(DEB_SRCDIR)/.config $(DEB_SRCDIR)/.config-$(1); fi
pop_cfg_file  = if [ -f $(DEB_SRCDIR)/.config-$(1) ]; then mv -f $(DEB_SRCDIR)/.config-$(1) $(DEB_SRCDIR)/.config; fi

# Generate rdoc documentation for *-doc packages
# Just be sure you include debhelper.mk before including this file.
ifdef _cdbs_rules_debhelper
$(patsubst %,binary-install/%,$(DEB_RUBY_LIB_DOC_PACKAGES)) :: binary-install/% :
	dh_rdoc -p$(cdbs_curpkg)
endif

endif

