# Useful aliases (for bash), for Ruby Debian package development.
# See http://pkg-ruby-extras.alioth.debian.org/subversion.html for details.
alias svn-b='svn-buildpackage --svn-ignore --svn-builder ''debuild'' -us -uc'
alias svn-br='svn-b --svn-dont-purge --svn-reuse'
alias svn-e='svn-buildpackage --svn-export'
alias svn-pb='svn-buildpackage --svn-builder ''pdebuild --use-pdebuild-internal'''
alias svn-t='svn-buildpackage --svn-only-tag'
alias pkg-ruby-get-sources='uscan --download-current-version --destdir ../tarballs/'
