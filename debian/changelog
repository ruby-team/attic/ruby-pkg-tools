ruby-pkg-tools (0.21) UNRELEASED; urgency=medium

  [ Cédric Boutillier ]
  * Bump debhelper compatibility level to 9
  * Use https:// in Vcs-* fields
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Tue, 13 Aug 2019 06:35:55 +0530

ruby-pkg-tools (0.20) unstable; urgency=low

  * Team upload
  * Fix Vcs-* fields to point to the Git repository

 -- Cédric Boutillier <boutil@debian.org>  Tue, 15 Oct 2013 11:06:00 +0200

ruby-pkg-tools (0.19) unstable; urgency=low

  * Team upload
  * Fix encoding issues in bin/dh_rdoc causing a POD error (Closes: #724188)
  * Add explicit dependency on ruby1.8 as it is called explicitly
  * Bump Standards-Version to 3.9.4 (no changes needed)
  * Change dependency from libsetup-ruby1.8 to ruby-setup

 -- Cédric Boutillier <boutil@debian.org>  Tue, 15 Oct 2013 10:35:45 +0200

ruby-pkg-tools (0.18) unstable; urgency=low

  [ Antonio Terceiro ]
  * Call /usr/bin/ruby1.8 explicitly, instead of /usr/bin/ruby. ruby-pkg-tools
    will most probably not be ported to Ruby 1.9. This should fix FTBFS bugs
    on packages that build using ruby-pkg-tools.
  * debian/rules: create formatted manpage directly inside of installation
    directory.
  * debian/control: Bump Standards-Version, no changes needed.
  * Added debian/source/format

  [ Gunnar Wolf ]
  * Removing Thierry Reding from the group, as per mia-teammaint request
    (Closes: #572542)

 -- Antonio Terceiro <terceiro@debian.org>  Mon, 04 Jun 2012 21:45:13 -0300

ruby-pkg-tools (0.17) unstable; urgency=low

  [ Marc Dequènes (Duck) ]
  * Corrected package section.

  [ Lucas Nussbaum ]
  * Add 1.9.1 to the list of supported versions. 

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Tue, 19 Jan 2010 14:46:56 +1300

ruby-pkg-tools (0.16) unstable; urgency=low

  [ Micah Anderson ]
  * Add myself to the team

  [ Laurent Vallar ]
  * Add myself to the team

  [ Joshua Timberman ]
  * Add myself to the team

  [ Daiki Ueno ]
  * Add myself to the team

  [ Marco Rodrigues ]
  * Add myself to the team

  [ Ryan Niebur ]
  * remove the pkg-ruby-get-sources script, add entry in NEWS
  * remove pkg-ruby-extras.sources, it's no longer used
  * Debian Policy 3.8.3 (no changes)
  * regenerate the manpage from POD during package build

 -- Ryan Niebur <ryanryan52@gmail.com>  Fri, 04 Sep 2009 13:32:49 -0700

ruby-pkg-tools (0.15) unstable; urgency=low

  [ Michael Schutte ]
  * pkg-ruby-get-sources: Support downloading from FTP servers.
  * 1/class/ruby-common.mk: Set LIB_PACKAGE_FILTER and LIB_DOC_PACKAGE_FILTER
    with ?= instead of =.  Individual packages might want to override these
    settings to build, for example, empty transitional packages.
  * Add myself to the team.

  [ Ryan Niebur ]
  * Add me to the team.
  * instead of ignoring all errors from make distclean, only run it if
    the makefile exists
  * add myself to uploaders
  * debhelper compat 5
  * debian policy 3.8.1
  * misc:Depends
  * remove Florian Ragwitz from Uploaders, (Closes: #523202)
  * add a dh_rdoc sequence for debhelper

  [ Daniel Moerner ]
  * Add myself to the team.

 -- Ryan Niebur <ryanryan52@gmail.com>  Wed, 01 Jul 2009 20:42:29 -0700

ruby-pkg-tools (0.14) unstable; urgency=high

  [ Antonio Terceiro ]
  * debian/control: fix typo in package description. Thanks to Kobayashi
    Noritada (Closes: #471517)

  [ Lucas Nussbaum ]
  * extconf class: install ruby 1.9 libraries to /usr/lib/ruby/1.9.0, not
    /usr/lib/ruby/1.9. If your package uses this class and builds libs for
    1.9, you must build-depend on ruby-pkg-tools >= 0.14. Closes: #484611.
  * Urgency=high, the upload fixes an RC bug.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Mon, 16 Jun 2008 14:27:09 +0200

ruby-pkg-tools (0.13) unstable; urgency=low

  [ Antonio Terceiro ]
  * 1/class/ruby-setup-rb.mk: using --installdirs=std as setup.rb options, so
    everything is put in the right place upon installation (Closes: #420028)
  * bin/pkg-ruby-get-sources: provide a better progress update information,
    compatible with narrow terminals (Closes: #422536)
  * debian/control: new maintainer name and address for myself
  * 1/class/ruby-setup-rb.mk: installing a symbolic link to packaged setup.rb
    in build directory root when maintainer wants to use setup.rb to build
    the package. All packages shall be built using the packaged setup.rb,
    event if it already provides one. This way we can e.g. fork setup.rb in
    the future if Debian needs some modification in the build process.
    Thanks to Gunnar Wolf for the idea and initial patch.
  * debian/control: added depencency on libsetup-ruby1.8

  [ Stig Sandbeck Mathisen ]
  * Added myself to the team

  [ Lucas Nussbaum ]
  * Added Vcs-Svn and Vcs-Browser.
  * fixed Vcs-*: point to dir containing debian/, not dir containing
    trunk/

  [ Arnaud Cornet ]
  * 1/class/ruby-setup-rb.mk: Add $(DEB_RUBY_INSTALL_SETUP_CMD) when building
    regular library packages.
  * Enforce use of libsetup-ruby1.8's setup.rb for all packages.

  [ Lucas Nussbaum ]
  * Updated to policy 3.7.3. No changes needed.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Tue, 12 Feb 2008 22:55:41 +0100

ruby-pkg-tools (0.12) unstable; urgency=low

  [ Lucas Nussbaum ]
  * Changed a nobreakspace character to a space in the source.

  [ Maxim Komar ]
  * Added myself to the team.

  [ Arnaud Cornet ]
  * Add a simple check that might save you some work if you include
    uploaders.mk and forget to create a control.in file.

  [ Paul van Tilburg ]
  * Added a warning to uploaders.mk since it is deprecated as of this
    release!
  * Removed source for libcairo-ruby (unneeded) and libgpgme-ruby (hosted
    on RubyForge now) from the deprecated sources list.
  * Remove 'dehs' output fix for uscan in bin/pkg-ruby-get-sources.

 -- Paul van Tilburg <paulvt@debian.org>  Wed, 04 Jul 2007 22:56:11 +0200

ruby-pkg-tools (0.11.5) unstable; urgency=low

  * Added myself to the team.

 -- Gunnar Wolf <gwolf@debian.org>  Tue,  2 Jan 2007 14:04:07 -0600

ruby-pkg-tools (0.11.4) unstable; urgency=low

  * Added myself to the team.

 -- Florian Ragwitz <rafl@debian.org>  Mon, 13 Nov 2006 00:07:29 +0100

ruby-pkg-tools (0.11.3) unstable; urgency=low

  * Added myself to the team.

 -- Filipe Lautert <filipelautert@celepar.pr.gov.br>  Thu,  9 Nov 2006 14:23:43 -0200

ruby-pkg-tools (0.11.2) unstable; urgency=low

  * Removed rcov and libgems-ruby from the sources file, since they
    have watch files.
  * Fix DEB_RUBY_ARCHDIR in ruby-common.mk to call the current ruby version
    instead of just ruby, so installing an extension will work with ruby 1.9.
  * In dh_rdoc, read entire .rdoc file instead of just one line

 -- Ari Pollak <ari@debian.org>  Sat,  7 Oct 2006 14:26:56 -0400

ruby-pkg-tools (0.11.1) unstable; urgency=low

  * Bugfix release.
  * Applied patch for ruby-common.mk to fix the path where extensions were
    installed. Patch courtesy of Sjoerd Simons (closes #390757).
  * Performed (temporary) dirty hack in pkg-ruby-get-sources to make it
    parse the DEHS output of uscan again.

 -- Paul van Tilburg <paulvt@debian.org>  Wed,  4 Oct 2006 21:50:36 +0200

ruby-pkg-tools (0.11) unstable; urgency=low

  [ Rudi Cilibrasi ]
  * Glad to be part of the team.

  [ Vincent Fourmond ]
  * Added myself to pkg-ruby-extras.team.

  [ Patrick Ringl ]
  * Added myself to the team aswell.

  [ Esteban Manchado Velázquez ]
  * Add "rdoc" to Depends, because of "dh_rdoc".
  * Add --siterubyver to DEB_RUBY_CONFIG_ARGS, so hobix (and others, probably)
    install things under /usr, instead of under /usr/local.
  * Add pkg-ruby-extras.sh, with useful aliases.

  [ Paul van Tilburg ]
  * Updated the package description.
  * Removed my packages from the sources file.

  [ Lucas Nussbaum ]
  * Removed my packages from the sources file, since they all use watch files.

  [ Arnaud Cornet ]
  * Removed my packages from the sources file, since they all use watch files.

 -- Arnaud Cornet <arnaud.cornet@gmail.com>  Sun, 10 Sep 2006 00:30:37 +0200

ruby-pkg-tools (0.10) unstable; urgency=low

  [ Lucas Nussbaum ]
  * Added source for librmagick-ruby version 1.12.0.
  * Added source for libgsl-ruby version 1.8.3.

  [ Paul van Tilburg ]
  * Added missing comma in the team list, leading to erronous control files
    being generated with version 0.9 of the package tools.

  [ Ari Pollak ]
  * Change Build-Depends-Indep: cdbs, debhelper to Build-Depends,
    as per policy section 7.6

  [ Esteban Manchado Velázquez ]
  * ruby-common.mk:
    * Fix DEB_RUBY_LIBDIR hardcodedness, and strip it.
    * Drop support for Ruby 1.6.
    * Avoid rb files compression, if using debhelper. Thanks to Lucas Nussbaum
      for the idea and code.
    * Also define a DEB_RUBY_ARCHDIR variable.
  * ruby-get-sources:
    * Drop support for sources file.
    * Some code refactoring.
  * ruby-extconf-rb.mk:
    * Specify both sitelibdir and sitearchdir (Closes: #360986). Thanks to
      Sjoerd Simons, the submitter, for the patch.
  * debian/control.in:
    * Bumped Standars-Version to 3.7.2 (no changes).

 -- Esteban Manchado Velázquez <zoso@debian.org>  Mon, 26 Jun 2006 22:29:36 +0100

ruby-pkg-tools (0.9) unstable; urgency=low

  [ Ari Pollak ]
  * Added source for librcov-ruby.

  [ Paul van Tilburg ]
  * Set the Maintainer field back to me.  I will be mainly responsible for
    this package.

  [ Lucas Nussbaum ]
  * Added another strategy for fetching the upstream tarball: running
    uscan --dehs and parsing the output.
  * Added librmagick-ruby source for version 1.11.0.

  [ Arnaud Cornet ]
  * Added sources for libdev-utils-ruby and libextensions-ruby.

  [ Paul van Tilburg ]
  * Usage of the packaged or online "pkg-ruby-extras.sources" file is
    deprecated as of this version!  Support will be removed in the next
    upload/version.

 -- Paul van Tilburg <paulvt@debian.org>  Tue, 23 May 2006 20:57:18 +0200

ruby-pkg-tools (0.8) unstable; urgency=low

  [ Paul van Tilburg ]
  * Updated the sources file to use the upstream-cache for clean tarballs
    of facets (upstream original missing) and commandline (contained CVS dirs).

  [ Thierry Reding ]
  * Added a configurable variable DEB_RUBY_CLEAN_TARGET to the ruby-setup.rb
    CDBS class which can be used to override the default clean command
    (distclean) passed to the setup.rb/install.rb script.
  * Now runs `config' before each `clean' to avoid failures caused by missing
    configuration.
  * Added code to the pkg-ruby-get-sources to use a package's watch file as
    primary source to find upstream source tarballs.
  * uploaders.mk now replaces both @RUBY_TEAM@ and @RUBY_EXTRAS_TEAM@ markers
    by the contents of pkg-ruby-extras.team, issuing a warning if it finds
    @RUBY_TEAM@ (which is now deprecated).

  [ Ari Pollak ]
  * Added myself to pkg-ruby-extras.team

 -- Thierry Reding <thierry@doppeltgemoppelt.de>  Wed, 22 Mar 2006 13:16:06 +0100

ruby-pkg-tools (0.7) unstable; urgency=low

  [ Lucas Nussbaum ]
  * Added source for libgnuplot-ruby.
  * Added source for librmagick-ruby.
  * Added source for feed2imap 0.7.

  [ Thierry Reding ]
  * pkg-ruby-get-sources:
     + Now also supports tgz and bzipped archives:
        - tgz archives are automatically renamed to tar.gz
        - bzipped archives are converted to gzipped archives
     + No longer displays the verbose message `I: Found source tarball...'
       when not in verbose mode.
     + Added support for HTTP redirections.
  * Added upstream tarball for the liblog4r-ruby package.
  * Added upstream tarball for the libgpgme-ruby package.
  * The CDBS class now correctly builds library packages for different Ruby
    versions.
  * Added a CDBS class for extconf.rb based Ruby packages. I've also split off
    some of the common code from the setup.rb class so that it can be shared
    by both ruby-setup-rb and ruby-extconf-rb classes.

 -- Thierry Reding <thierry@doppeltgemoppelt.de>  Fri, 24 Feb 2006 18:56:24 +0100

ruby-pkg-tools (0.6) unstable; urgency=low

  [ Lucas Nussbaum ]
  * Added source for feed2imap.

  [ Marc Dequènes (Duck) ]
  * Fixed CDBS class for 'simple packages' case.
  * Ensure we use debhelper scripts only if debhelper class was loaded
    before.
  * Removed unneeded variables : DEB_RUBY_CLEAN_ARGS,
    DEB_RUBY_SETUP_ARGS, and DEB_RUBY_INSTALL_ARGS_ALL (based on
    setup.rb and install.rb capabilities).

  [ Thierry Reding ]
  * pkg-ruby-get-sources:
     + The script is now more modularized, which made the following steps
       easier to implement.
     + Now uses pkg-ruby-extras.sources from the ruby-pkg-tools package as
       default sources file, falling back to the sources file on the project
       website if a tarball URL for the given package is not found in the
       local copy.
     + The `--sources' switch can now also handle HTTP, HTTPS and FTP URLs.
     + The script now also has a vi modeline. =)

 -- Marc Dequènes (Duck) <Duck@DuckCorp.org>  Thu,  5 Jan 2006 23:14:40 +0100

ruby-pkg-tools (0.5) unstable; urgency=low

  [ Esteban Manchado Velázquez ]
  * Added dh_rdoc script.
  * Added ruby-setup-rb.mk hooks to automatically call dh_rdoc for
    documentation packages.

  [ Thierry Reding ]
  * Added myself to the team.
  * Added upstream tarball for the libcairo-ruby package.
  * pkg-ruby-get-sources:
     + no longer checks for the existence of $target_directory if the
       --list-available option is specified.
  * Added a missing comma to the `pkg-ruby-extras.team' file.

  [ Marc Dequènes (Duck) ]
  * Added myself to the team.

  [ Paul van Tilburg ]
  * Added man page for the dh_rdoc script.

 -- Thierry Reding <thierry@doppeltgemoppelt.de>  Wed,  4 Jan 2006 22:06:49 +0100

ruby-pkg-tools (0.4) unstable; urgency=low

  [ Lucas Nussbaum ]
  * Added myself to the team.
  * Added source for libxmpp4r-ruby.

  [ Esteban Manchado Velázquez ]
  * ruby-setup-rb.mk class cleanup:
    - added Ruby version 1.6 support,
    - fixed problems with building non-lib packages,
    - used -f switch in mv when moving around .config files.
  * Enhanced ruby-setup-rb.mk compatibility with the install.rb script.
  * Added source for libjson-ruby.

  [ Paul van Tilburg ]
  * Updated the source entry for the Cmd library.
  * Added source entry for the Daemonize library.

  [ Arnaud Cornet ]
  * Added libcmdparse2-ruby to pkg-ruby-extras.sources.

 -- Paul van Tilburg <paulvt@debian.org>  Sat, 17 Dec 2005 13:36:12 +0000

ruby-pkg-tools (0.3) unstable; urgency=low

  [ Paul van Tilburg ]
  * Added sources for libbreakpoint-ruby, libdbus-ruby, libfacets-ruby and
    libsvg-ruby.

  [ Esteban Manchado Velázquez ]
  * Minor fixes in ruby-setup-rb.mk. Thanks to Paul van Tilburg.
  * Fixed ruby-setup-rb.mk copyright.
  * Added myself to uploaders.

  [ Arnaud Cornet ]
  * Added myself to the team

 -- Arnaud Cornet <arnaud.cornet@gmail.com>  Tue, 18 Oct 2005 22:22:07 +0200

ruby-pkg-tools (0.2) unstable; urgency=low

  * Resolved lintian warnings and errors:
    - Added man page for pkg-ruby-getsources.
    - Removed the #!/usr/bin/make from uploaders.mk.
    - Changed Build-Depends to Build-Depends-Indeps.
  * Added cdbs as a Suggest.
  * Fixed setup.rb config --prefix error in ruby-setup-rb.mk.

 -- Paul van Tilburg <paulvt@debian.org>  Sun,  4 Sep 2005 19:29:06 +0200

ruby-pkg-tools (0.1) unstable; urgency=low

  * Initial Release.

 -- Paul van Tilburg <paulvt@debian.org>  Sun, 28 Aug 2005 11:40:32 +0200
